<?php

namespace addons\sdcmenu\model;

class Carousel extends Model
{
    // 表名
    protected $name = 'sdcmenu_carousel';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'category_text'
    ];

    

    protected static function init()
    {
        self::afterInsert(function ($row){
            $row->save(['weigh' => $row['id']]);
        });
    }


    public function getCategoryTextAttr($value,$data)
    {
        $config = get_addon_config(Model::AddonName);
        return isset($config['carousel'][$data['category']]) 
        ? $config['carousel'][$data['category']] : '-';
    }

}
